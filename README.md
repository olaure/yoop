# Techs

- FastAPI
- Uvicorn to serve
- Docker to have the mongo database

## Requirements

docker && poetry


## Usage

To serve the API, use `make serve`. It needs docker to run properly.

The other make commands are `up`, `clean`, `test`

## Tests

Only basic tests are implemented so far.

Api response and db storage are evaluated.

Vacations merges are checked.

No edge/corner cases are tested although the code should not fail for the obvious ones.


## Features

- Team, Employee and vacation are objects taht can be created and updated.
- Teams cannot be deleted (but they can be renamed).
- Vacations are merged as expected when there is no workday worked between two vacations of the same kind.


## Comments

It was fun to do, although I wish I had more time to spend on the search feature and the tests. 
There are a lot of edge cases I can think of and clearly not enough tests to handle them all. 
I initially thought about using GraphQL but time was missing and I resorted to a more conventional API style.

TBH, I used the poetry lock mostly to expose what I am working with, a better architecture would also install them in a virtualenv or even in a containerized environment for tests and setup.
