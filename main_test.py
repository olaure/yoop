import os
from typing import Dict


os.environ["env"] = "test"

from fastapi.testclient import TestClient
import pytest

from main import app
from src.database import teams_table, employees_table, vacations_table

TESTER = TestClient(app)

@pytest.fixture
def clean():
    teams_table().drop()
    employees_table().drop()
    vacations_table().drop()

def create_employee(employee: Dict) -> Dict:
    team_name = employee["team"]
    team_response = TESTER.put("/team/new", json={"name": team_name})
    emp_response = TESTER.put("/employees/new", json=employee)
    emp = emp_response.json()["employees"][0]
    return emp


def test_teams(clean):
    response = TESTER.get("/teams")
    assert response.status_code == 200
    assert response.json() == {"teams": [], "result": "success"}

def test_new_team(clean):
    response = TESTER.put("/team/new", json={"name": "team_name"})
    assert response.status_code == 200
    js = response.json()
    assert js["result"] == "success"
    assert len(js["teams"]) == 1
    assert js["teams"][0]["name"] == "team_name"

def test_edit_team(clean):
    response = TESTER.put("/team/new", json={"name": "team_name"})
    print(response.json())
    tid = response.json()["teams"][0]["_id"]
    response = TESTER.post("/team/{}/edit".format(tid), json={"name": "team_name2"})
    print(response.json())
    assert response.status_code == 200
    js = response.json()
    assert js["result"] == "success"
    assert len(js["teams"]) == 1
    assert js["teams"][0]["name"] == "team_name2"
    # TODO improve once team check is done



def test_all_employee(clean):
    response = TESTER.get("/employees")
    assert response.status_code == 200
    assert response.json() == {"employees": [], "result": "success"}

def test_new_employee(clean):
    team_response = TESTER.put("/team/new", json={"name": "team_name"})
    response = TESTER.put("/employees/new", json={"name": "first last", "team": "team_name"})
    assert response.status_code == 200
    js = response.json()
    print(js)
    assert js["result"] == "success"
    assert len(js["employees"]) == 1
    assert js["employees"][0]["name"] == "first last"
    assert js["employees"][0]["team_id"] == team_response.json()["teams"][0]["_id"]

def test_edit_employee(clean):
    team_response = TESTER.put("/team/new", json={"name": "team_name"})
    emp_response = TESTER.put("/employees/new", json={"name": "first last", "team": "team_name"})
    eid = emp_response.json()["employees"][0]["_id"]
    response = TESTER.post("/employees/{}/edit".format(eid), json={"name": "last first"})
    assert response.status_code == 200
    js = response.json()
    assert js["result"] == "success"
    assert len(js["employees"]) == 1
    assert js["employees"][0]["name"] == "last first"
    assert js["employees"][0]["team_id"] == team_response.json()["teams"][0]["_id"]

def test_delete_employee(clean):
    team_response = TESTER.put("/team/new", json={"name": "team_name"})
    emp_response = TESTER.put("/employees/new", json={"name": "first last", "team": "team_name"})
    emp = emp_response.json()["employees"][0]
    response = TESTER.delete("/employees/{}".format(emp["_id"]))
    assert response.status_code == 200
    assert response.json() == {"employees": [emp], "result": "success"}


VACATIONS_FORMAT_ALL = "/employees/{}/vacations".format
VACATIONS_FORMAT_NEW = "/employees/{}/vacations/new".format
VACATIONS_FORMAT_EDIT = "/employees/{}/vacations/{}/edit".format
VACATIONS_FORMAT_DEL = "/employees/{}/vacations/{}".format

def test_all_vacation(clean):
    emp = create_employee({"team": "team name", "name": "first last"})
    emp_id = emp["_id"]
    response = TESTER.get(VACATIONS_FORMAT_ALL(emp_id))
    assert response.status_code == 200
    assert response.json() == {"employees": [{"_id": emp_id,"vacations":[]}], "result": "success"}

def test_new_vacation(clean):
    emp = create_employee({"team": "team name", "name": "first last"})
    emp_id = emp["_id"]
    response = TESTER.put(VACATIONS_FORMAT_NEW(emp_id), json={"kind": "normal", "start": "2020-12-01", "end": "2020-12-31"})
    assert response.status_code == 200
    js = response.json()
    print(js)
    assert js == {"employees": [{"_id": emp_id, "vacations":[{
        "kind": "normal",
        "start": "2020-12-01",
        "end": "2020-12-31",
        "employee_id": emp_id,
        "_id": js["employees"][0]["vacations"][0]["_id"],
    }]}], "result": "success"}

def test_edit_vacation(clean):
    emp = create_employee({"team": "team name", "name": "first last"})
    emp_id = emp["_id"]
    vac_resp = TESTER.put(VACATIONS_FORMAT_NEW(emp_id), json={"kind": "normal", "start": "2020-12-01", "end": "2020-12-31"})

    orig_vac = vac_resp.json()["employees"][0]["vacations"][0]
    vac_id = orig_vac["_id"]

    response = TESTER.post(VACATIONS_FORMAT_EDIT(emp_id, vac_id), json={"kind": "rtt"}) # Lowercase would fail
    assert response.status_code == 200
    orig_vac["kind"] = "rtt"
    assert response.json() == {"employees": [{"_id": emp_id, "vacations":[orig_vac]}], "result": "success"}

def test_delete_vacation(clean):
    emp = create_employee({"team": "team name", "name": "first last"})
    emp_id = emp["_id"]

    vac_resp = TESTER.put(VACATIONS_FORMAT_NEW(emp_id), json={"kind": "normal", "start": "2020-12-01", "end": "2020-12-31"})

    orig_vac = vac_resp.json()["employees"][0]["vacations"][0]
    vac_id = orig_vac["_id"]

    response = TESTER.delete(VACATIONS_FORMAT_DEL(emp_id, vac_id))
    assert response.status_code == 200
    assert response.json() == {"employees": [{"_id": emp_id, "vacations":[orig_vac]}], "result": "success"}

def test_overlapping_vacation(clean):
    emp = create_employee({"team": "team name", "name": "first last"})

    emp_id = emp["_id"]
    first = {"kind": "normal", "start": "2020-12-01", "end": "2020-12-31"}
    second = {"kind": "normal", "start": "2020-11-01", "end": "2020-12-03"}
    first_resp = TESTER.put(VACATIONS_FORMAT_NEW(emp_id), json=first)
    second_resp = TESTER.put(VACATIONS_FORMAT_NEW(emp_id), json=second)

    first_vac = first_resp.json()["employees"][0]["vacations"][0]
    second_vac = second_resp.json()["employees"][0]["vacations"][0]

    assert second_vac == {
        "kind": "normal",
        "_id": first_vac["_id"],
        "employee_id": emp_id,
        "start": "2020-11-01",
        "end": "2020-12-31",
    }

def test_search(clean):
    emp1 = create_employee({"team": "team name", "name": "first last"})
    emp2 = create_employee({"team": "team name", "name": "last first"})

    emp1_id = emp1["_id"]
    emp2_id = emp2["_id"]

    first = {"kind": "normal", "start": "2020-12-01", "end": "2020-12-31"}
    second = {"kind": "normal", "start": "2020-11-01", "end": "2020-12-03"}
    first_resp = TESTER.put(VACATIONS_FORMAT_NEW(emp1_id), json=first).json()
    second_resp = TESTER.put(VACATIONS_FORMAT_NEW(emp2_id), json=second).json()
    emp1["vacations"] = first_resp["employees"][0]["vacations"]
    emp2["vacations"] = second_resp["employees"][0]["vacations"]

    first_vac = first_resp["employees"][0]["vacations"][0]
    second_vac = second_resp["employees"][0]["vacations"][0]

    search = {"after": "2020-12-01", "before": "2020-12-05"}
    result = TESTER.post("/employees/search", json=search)

    assert result.status_code == 200
    js = result.json()
    assert js["employees"] == [emp1, emp2] or [emp2, emp1]


## TODO tests
# default vacation
# errors (unknown Vacation Kind, vacation of another employee, end before start)
# Edge cases
# Overlapping vacations (frank overlapping, day to tomorrow, weekend between)
