from typing import Dict, Optional
from uuid import uuid4

from .database import employees_table
from .database import EntryExists, EntryAbsent, UpdateFailed

from .model import Employee, EmployeeEdit

from .teams import team_get

def get_all_employees():
    return list(employees_table().find())

def employee_get(employee_name: str="", employee_id: str="") -> Optional[Dict]:
    query = {}
    if employee_name:
        query["name"] = employee_name
    if employee_id:
        query["_id"] = employee_id
    if not query:
        return None
    return employees_table().find_one(query)

def employee_exists(employee_name: str="", employee_id: str="") -> bool:
    return bool(employee_get(employee_name=employee_name, employee_id=employee_id))

def employee_insert(employee: Employee):
    if employee_exists(employee_name=employee.name):
        raise EntryExists("employee {} already exists".format(employee.name))
    team = team_get(team_name=employee.team)
    if not team:
        raise EntryAbsent("team {} does not exist".format(employee.team))

    entry = {
        "_id": str(uuid4()),
        "name": employee.name,
        "team_id": team["_id"]
    }
    r = employees_table().insert_one(entry)
    if not r.acknowledged:
        raise UpdateFailed("Failed to insert employee {}: UNACKed".format(employee.name))
    return entry

def employee_edit(employee_id: str, edit: EmployeeEdit):
    old_employee = employee_get(employee_id=employee_id)
    if not old_employee:
        raise EntryAbsent("employee {} does not exists".format(employee_id))

    team = team_get(team_name=edit.team)
    if edit.team and not team:
        raise EntryAbsent("team {} does not exist".format(edit.team))

    new_data = {}
    if edit.name:
        new_data["name"] = edit.name
    if edit.team:
        new_data["team_id"] = team["_id"]
    if not new_data:
        raise UpdateFailed("Empty edit for employee {}".format(employee_id))

    r = employees_table().update_one({"_id": employee_id}, {"$set": new_data})
    if not r.acknowledged:
        raise UpdateFailed("Failed to edit employee {}: UNACKed".format(employee_id))
    return {
        "_id": employee_id,
        "name": new_data.get("name") or old_employee["name"],
        "team_id": new_data.get("team_id") or old_employee["team_id"]
    }

def employee_delete(employee_id: str):
    old_employee = employee_get(employee_id=employee_id)
    if not old_employee:
        raise EntryAbsent("employee {} does not exists".format(employee_id))

    r = employees_table().delete_one({"_id": employee_id})
    if not r.acknowledged:
        raise UpdateFailed("Failed to edit employee {}: UNACKed".format(employee_id))
    return old_employee
