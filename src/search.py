from collections import defaultdict
import datetime as dt
import re
from typing import Dict, List

from .database import vacations_table

from .model import EmployeeSearch

from .employees import employee_get
from .vacations import format_vacation


def get_vacated(start: dt.date, end: dt.date) -> List[Dict]:
    start_datetime = dt.datetime.combine(start, dt.time())
    end_datetime = dt.datetime.combine(end, dt.time())
    query = {
        "$or": [
            {
                "start": {
                    "$gte": start_datetime,
                    "$lte": end_datetime,
                }
            },{
                "end": {
                    "$gte": start_datetime,
                    "$lte": end_datetime,
                }
            }
        ]
    }
    return list(vacations_table().find({
        "$query": query,
        "$orderby": {"start": 1},
    }))

def employee_search(search: EmployeeSearch):
    """This is done in a hacky way.
    If we had a sql database, it would be done using joints.
    Although it would put a strain on the db when searching the employees.
    """
    if search.after > search.before:
        raise ValueError("vacation needs to end after it started")

    vacations = get_vacated(search.after, search.before)

    vacations_by_employee = defaultdict(list)
    for vac in vacations:
        vacations_by_employee[vac["employee_id"]].append(format_vacation(vac))

    selected = []
    if search.name:
        pattern = re.compile("*{}*".format(re.escape(search.name)))
    for emp_id, vacs in vacations_by_employee.items():
        employee = employee_get(employee_id=emp_id)
        if not search.name or pattern.match(employee["name"]):
            employee["vacations"] = vacs
            selected.append(employee)

    return selected
