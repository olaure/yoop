import os

import pymongo

CLIENT = pymongo.MongoClient("127.0.0.1:5517")
TABLE = CLIENT["orm"]

class EntryExists(Exception):
    pass

class EntryAbsent(Exception):
    pass

class UpdateFailed(Exception):
    pass


def teams_table():
    table = "orm" if os.environ.get("env") != "test" else "test"
    return CLIENT[table]["teams"]

def employees_table():
    table = "orm" if os.environ.get("env") != "test" else "test"
    return CLIENT[table]["employees"]

def vacations_table():
    table = "orm" if os.environ.get("env") != "test" else "test"
    return CLIENT[table]["vacations"]
