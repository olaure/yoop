import datetime as dt
from typing import Dict, List, Optional, Tuple
from uuid import uuid4

from .database import vacations_table, CLIENT
from .database import EntryExists, EntryAbsent, UpdateFailed

from .model import Vacation, VacationEdit, VacationKind

from .employees import employee_get, employee_exists

def format_vacation(entry: Dict) -> Dict:
    return {
        "_id": entry["_id"],
        "kind": entry["kind"],
        "employee_id": entry["employee_id"],
        "start": entry["start"].date(),
        "end": entry["end"].date(),
    }

def vacation_get(employee_id: str, vacation_id: str="") -> Optional[Dict]:
    query = {}
    if vacation_id:
        query["_id"] = vacation_id
    if not query:
        return None
    query["employee_id"] = employee_id
    return vacations_table().find_one(query)

def vacation_exists(employee_id: str, vacation_id: str="") -> bool:
    return bool(vacation_get(employee_id, vacation_id=vacation_id))

def search_overlaps(employee_id: str, vacation_kind: str, start: dt.date, end: dt.date) -> List[Dict]:
    """This is the lookup for overlaps.
    Overlaps include range overlaps, adjacent days and adjacent work days (friday and monday)
    """
    # Let's take the days right before and after to merge adjacent vacations
    start_datetime = dt.datetime.combine(start, dt.time()) - dt.timedelta(days=1)
    end_datetime = dt.datetime.combine(end, dt.time()) + dt.timedelta(days=1)
    # Let's consider the weekend before
    if start_datetime.isoweekday() > 5:
        start_datetime = start_datetime - dt.timedelta(days=start_datetime.isoweekday() - 5)
    # Let's consider the weekend after
    if end_datetime.isoweekday() > 5:
        end_datetime = end_datetime + dt.timedelta(days=8 + start_datetime.isoweekday())
    query = {
        "$or": [
            {
                "$and": [
                    {"employee_id": employee_id},
                    {"kind": vacation_kind.value},
                    {
                        "start": {
                            "$gte": start_datetime,
                            "$lte": end_datetime,
                        }
                    },
                ]
            },{
                "$and": [
                    {"employee_id": employee_id},
                    {"vacation_kind": vacation_kind.value},
                    {
                        "end": {
                            "$gte": start_datetime,
                            "$lte": end_datetime,
                        }
                    },
                ]
            }
        ]
    }
    return list(vacations_table().find({
        "$query": query,
        "$orderby": {"start": 1},
    }))

def merge_overlaps(employee_id: str, vacation: Vacation, overlaps: List[Dict], vacation_kind: VacationKind, vacation_id: Optional[str] = None) -> Tuple[Dict, List[Dict]]:
    """This method merges the given overlapping vacations.
    If no overlaps, creates a new vaacation.
    Otherwise, the vacation with the earliest start_date will be modified and the other deleted.
    """
    if not overlaps:
        return {
            "_id": vacation_id or str(uuid4()),
            "kind": vacation_kind,
            "employee_id": employee_id,
            "start": dt.datetime.combine(vacation.start, dt.time()),
            "end": dt.datetime.combine(vacation.end, dt.time()),
        }, []
    earliest_start = min(vacation.start, *[x["start"].date() for x in overlaps])
    latest_end = max(vacation.end, *[x["end"].date() for x in overlaps])
    return {
        "_id": overlaps[0]["_id"],  # overlaps are sorted by start. We choose always to update the first one
        "kind": vacation_kind,
        "employee_id": employee_id,
        "start": dt.datetime.combine(earliest_start, dt.time()),
        "end": dt.datetime.combine(latest_end, dt.time()),
    }, [x for x in overlaps[1:]]

def update_vacations(entry: Dict, to_delete: List[Dict]) -> List[Dict]:
    """This method is written poorly and could lead to an instable state.
    However, the proper code using a transaction doesn't work on a one-node mongo setup.
    Fortunately, the probability that a mongo node running corrupted or a single of those operations failing
    while in such a test setup as this one is almost None.
    A good solution would be to setup a replication and the transaction could be used.
    """
    table = vacations_table()
    r = table.update_one({"_id": entry["_id"]}, {"$set": entry}, upsert=True)
    for item in to_delete:
        r = table.delete_one({"_id": item["_id"]})
    #try:
    #    with CLIENT.start_session() as session:
    #        with session.start_transaction():
    #            table = vacations_table()
    #            r = table.update_one({"_id": entry["_id"]}, {"$set": entry}, upsert=True, session=session)
    #            for item in to_delete:
    #                r = table.delete_one({"_id": item["_id"]}, session=session)
    #except Exception as err:
    #    print("FAILURE", err)
    #    raise UpdateFailed("Failed to upsert vacation: {}".format(err))
    return [format_vacation(e) for e in [entry, *to_delete]]

##############################
# Vacations routes responses #
##############################

def get_all_vacations(employee_id) -> List[Dict]:
    if not employee_exists(employee_id=employee_id):
        raise EntryAbsent("employee {} does not exist".format(employee_id))
    return [
        format_vacation(v)
        for v in vacations_table().find({"employee_id": employee_id})
    ]

def vacation_insert(employee_id: str, vacation: Vacation) -> List[Dict]:
    if not employee_exists(employee_id=employee_id):
        raise EntryAbsent("employee {} does not exist".format(employee_id))

    if vacation.end < vacation.start:
        raise ValueError("vacation needs to end after it started")

    # The default of normal vacations
    vacation_kind = vacation.kind or VacationKind.normal

    # Check near vacations
    overlaps = search_overlaps(employee_id, vacation_kind, vacation.start, vacation.end)

    entry, to_delete = merge_overlaps(employee_id, vacation, overlaps, vacation_kind)

    return update_vacations(entry, to_delete)

def vacation_edit(employee_id: str, vacation_id: str, edit: VacationEdit):
    employee = employee_get(employee_id=employee_id)
    if not employee:
        raise EntryAbsent("employee {} does not exist".format(employee_id))

    old_vacation = vacation_get(employee_id, vacation_id=vacation_id)
    if not old_vacation:
        raise EntryAbsent("vacation {} does not exists".format(vacation_id))

    new_data = {}
    if edit.kind:
        new_data["kind"] = edit.kind
    if edit.start:
        new_data["start"] = edit.start
    if edit.end:
        new_data["end"] = edit.end
    if not new_data:
        raise UpdateFailed("Empty edit for vacation {}".format(vacation_id))

    new_vacation = old_vacation.copy()
    new_vacation.update(new_data)
    vacation = Vacation(**new_vacation)

    if vacation.end < vacation.start:
        raise ValueError("vacation needs to end after it started")

    # Check near vacations
    overlaps = search_overlaps(employee_id, vacation.kind, vacation.start, vacation.end)

    entry, to_delete = merge_overlaps(employee_id, vacation, overlaps, vacation.kind, vacation_id)

    return update_vacations(entry, to_delete)



    #r = vacations_table().update_one({"_id": vacation_id}, {"$set": new_data})
    #if not r.acknowledged:
    #    raise UpdateFailed("Failed to edit vacation {}: UNACKed".format(vacation_id))
    #return format_vacation({
    #    "_id": vacation_id,
    #    "kind": new_data.get("kind") or old_vacation["kind"],
    #    "employee_id": employee_id,
    #    "start": new_data.get("start") or old_vacation["start"],
    #    "end": new_data.get("end") or old_vacation["end"],
    #})

def vacation_delete(employee_id: str, vacation_id: str):
    employee = employee_get(employee_id=employee_id)
    if not employee:
        raise EntryAbsent("employee {} does not exist".format(employee_id))

    old_vacation = vacation_get(employee_id, vacation_id=vacation_id)
    if not old_vacation:
        raise EntryAbsent("vacation {} does not exists".format(vacation_id))

    r = vacations_table().delete_one({"_id": vacation_id})
    if not r.acknowledged:
        raise UpdateFailed("Failed to edit vacation {}: UNACKed".format(vacation_id))
    return format_vacation(old_vacation)
