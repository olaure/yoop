from typing import Dict, Optional
from uuid import uuid4

from .database import teams_table
from .database import EntryExists, EntryAbsent

from .model import Team

def get_all_teams():
    return list(teams_table().find())

def team_get(team_name: str="", team_id: str="") -> Optional[Dict]:
    query = {}
    if team_name:
        query["name"] = team_name
    if team_id:
        query["_id"] = team_id
    if not query:
        return None
    return teams_table().find_one(query)

def team_exists(team_name: str="", team_id: str="") -> bool:
    return bool(team_get(team_name=team_name,team_id=team_id))

def team_insert(team: Team):
    if team_exists(team_name=team.name):
        raise EntryExists("team {} already exists".format(team.name))
    entry = {
        "_id": str(uuid4()),
        "name": team.name,
    }
    teams_table().insert_one(entry)
    return entry

def team_edit(team_id: str, team: Team):
    if not team_exists(team_id=team_id):
        raise EntryAbsent("team {} does not exists".format(team.name))
    teams_table().update_one({"_id": team_id}, {"$set": {"name": team.name}})
    return {
        "name": team.name,
        "_id": team_id,
    }
