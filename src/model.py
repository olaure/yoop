
import datetime
from enum import Enum
from typing import List, Optional

from pydantic import BaseModel


class Team(BaseModel):
    name: str

class Employee(BaseModel):
    name: str
    team: str

class EmployeeEdit(BaseModel):
    name: Optional[str]
    team: Optional[str]

class EmployeeSearch(BaseModel):
    name:   Optional[str]
    common: Optional[List[str]]
    # after and before are inclusive
    after:  datetime.date
    before: datetime.date

class VacationKind(str, Enum):
    normal = "normal"
    rtt = "rtt"
    unpaid = "unpaid"


class Vacation(BaseModel):
    start: datetime.date
    end: datetime.date
    kind: Optional[VacationKind]

class VacationEdit(BaseModel):
    start: Optional[datetime.date]
    end:   Optional[datetime.date]
    kind:  Optional[VacationKind]
