
PROJECT ?= worklife
MAIN_FILE ?= main
APP_NAME ?= app
VERSION ?= $(shell python3 ${PWD}/setup.py --version)


.PHONY: clean
clean:
	docker ps -qa --filter 'name=^/$(PROJECT)_.*' | xargs --no-run-if-empty docker rm --force

.PHONY: up
up: clean
	@docker run --name $(PROJECT)_mongo -d -p 127.0.0.1:5517:27017 mongo:4.4.2

.PHONY: serve
serve: up
	uvicorn ${MAIN_FILE}:${APP_NAME} --reload


.PHONY: test
test: up
	pytest -vv .
