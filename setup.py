#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages


install_requirements = ["sqlalchemy"]
setup_requirements = ['setuptools_scm==3.5.0']

setup(
    author="Oscar Laurent",
    install_requires=install_requirements,
    setup_requires=setup_requirements,
    include_package_data=True,
    name='orm',
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests",
                                    "*.test", "*.test.*", "test.*", "test"]),
    use_scm_version={"version_scheme": "python-simplified-semver"},
    zip_safe=False,
)
