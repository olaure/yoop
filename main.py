import logging
from fastapi import FastAPI, HTTPException


LOGGER = logging.getLogger("app")

#from src.routes import declare_routes

app = FastAPI()
#declare_routes(app)

from src.model import Team
from src.model import Employee, EmployeeEdit, EmployeeSearch
from src.model import Vacation, VacationEdit

from src.teams import get_all_teams, team_insert, team_edit
from src.employees import get_all_employees, employee_insert, employee_edit, employee_delete
from src.vacations import get_all_vacations, vacation_insert, vacation_edit, vacation_delete
from src.search import employee_search

from src.database import EntryExists, EntryAbsent, UpdateFailed


@app.get('/home')
def home():
    return {"message":"Welcome!"}

################
# Teams routes #
################

@app.get('/teams')
def all_teams():
    try:
        teams = get_all_teams()
    except Exception as err:
        raise HTTPException(status_code=503, detail=str(err))
    return {
        "teams": teams,
        "result": "success",
    }

@app.put('/team/new')
def new_team(team: Team):
    print("put team {}".format(team))
    try:
        result = team_insert(team)
    except EntryExists as err:
        raise HTTPException(status_code=409, detail=str(err))  #409 is edit conflict
    return {
        "teams": [result],
        "result": "success",
    }

@app.post('/team/{team_id}/edit')
def edit_team(team_id: str, team_ed: Team):
    print("change team {}: {}".format(team_id, team_ed))
    try:
        result = team_edit(team_id, team_ed)
    except EntryAbsent as err:
        raise HTTPException(status_code=404, detail=str(err))
    return {
        "teams": [result],
        "result": "success",
    }


####################
# Employees routes #
####################

@app.get('/employees')
def all_employee():
    print("all employee")
    try:
        employees = get_all_employees()
    except Exception as err:
        raise HTTPException(status_code=503, detail=str(err))
    return {
        "employees": employees,
        "result": "success",
    }

@app.put('/employees/new')
def new_employee(employee: Employee):
    print("new employee {}".format(employee))
    try:
        result = employee_insert(employee)
    except EntryExists as err:
        raise HTTPException(status_code=409, detail=str(err))  #409 is edit conflict
    except EntryAbsent as err:
        raise HTTPException(status_code=404, detail=str(err))
    return {
        "employees": [result],
        "result": "success",
    }

@app.post('/employees/{employee_id}/edit')
def edit_employee(employee_id: str, employee_ed: EmployeeEdit):
    print("edit employee {}: {}".format(employee_id, employee_ed))
    try:
        result = employee_edit(employee_id, employee_ed)
    except EntryAbsent as err:
        raise HTTPException(status_code=404, detail=str(err))
    except UpdateFailed as err:
        raise HTTPException(status_code=503, detail=str(err))
    return {
        "employees": [result],
        "result": "success",
    }

@app.delete('/employees/{employee_id}')
def delete_employee(employee_id: str):
    print("delete employee {}".format(employee_id))
    try:
        result = employee_delete(employee_id)
    except EntryAbsent as err:
        raise HTTPException(status_code=404, detail=str(err))
    except UpdateFailed as err:
        raise HTTPException(status_code=503, detail=str(err))
    return {
        "employees": [result],
        "result": "success",
    }

@app.post('/employees/search')
def search_employee(search: EmployeeSearch):
    print("Search employee {}".format(search))
    try:
        result = employee_search(search)
    except ValueError as err:
        raise HTTPException(status_code=400, detail=str(err))
    return {
        "employees": result,
        "result": "success",
    }


####################
# Vacations routes #
####################

@app.get('/employees/{employee_id}/vacations')
def all_vacation(employee_id: str):
    print("All vacations for employee {}".format(employee_id))
    try:
        vacations = get_all_vacations(employee_id)
    except EntryAbsent as err:
        raise HTTPException(status_code=404, detail=str(err))
    return {
        "employees": [{"_id": employee_id, "vacations": vacations}],
        "result": "success",
    }

@app.put('/employees/{employee_id}/vacations/new')
def new_vacation(employee_id: str, vacation: Vacation):
    print("New vacation for employee {}: {}".format(employee_id, vacation))
    try:
        result = vacation_insert(employee_id, vacation)
    except EntryExists as err:
        raise HTTPException(status_code=409, detail=str(err))  #409 is edit conflict
    except EntryAbsent as err:
        raise HTTPException(status_code=404, detail=str(err))
    except UpdateFailed as err:
        raise HTTPException(status_code=503, detail=str(err))
    except ValueError as err:
        raise HTTPException(status_code=400, detail=str(err))
    return {
        "employees": [{
            "_id": employee_id,
            "vacations": result,
        }],
        "result": "success",
    }

@app.post('/employees/{employee_id}/vacations/{vacation_id}/edit')
def edit_vacation(employee_id: str, vacation_id: str, vacation_ed: VacationEdit):
    print("Edit vacation {} for employee {}: {}".format(vacation_id, employee_id, vacation_ed))
    try:
        result = vacation_edit(employee_id, vacation_id, vacation_ed)
    except EntryAbsent as err:
        raise HTTPException(status_code=404, detail=str(err))
    except UpdateFailed as err:
        raise HTTPException(status_code=503, detail=str(err))
    except ValueError as err:
        raise HTTPException(status_code=400, detail=str(err))
    return {
        "employees": [{
            "_id": employee_id,
            "vacations": result,
        }],
        "result": "success",
    }

@app.delete('/employees/{employee_id}/vacations/{vacation_id}')
def delete_vacation(employee_id: str, vacation_id: str):
    print("Delete vacation {} for employee {}".format(vacation_id, employee_id))
    try:
        result = vacation_delete(employee_id, vacation_id)
    except EntryAbsent as err:
        raise HTTPException(status_code=404, detail=str(err))
    except UpdateFailed as err:
        raise HTTPException(status_code=503, detail=str(err))
    return {
        "employees": [{
            "_id": employee_id,
            "vacations":[result],
        }],
        "result": "success",
    }

